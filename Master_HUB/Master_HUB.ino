#include <Wire.h> // I2C bibliotek

// Konstanter
const int numberOfNeurons = 20; // Maks antall nevron i nettverket
const int numberOfVariables = 12;// Antall variabler pr nevron

// Liste over tilkoblingsstatus på nevronene. 1 indikerer tilkoblet. 0 indikerer frakoblet/ukjent
// Indeks samsvarer med addressen - 1. På adresse 0 svarer alle slavene, derfor uinteressant
// å lagre informasjon på denne adressen.
bool neuronStatus[numberOfNeurons];
<<<<<<< HEAD
// Dobbelt liste over alle variablene til nevronene
byte neuron_Variables[numberOfNeurons][numberOfVariables];

#define button_pin 14
#define pin_2E 4
#define pin_2I 5
#define pin_1E 6
#define pin_1I 7
#define pin_3E 16
#define pin_3I 10

int a{};
=======

// Dobbelt liste over alle variablene til nevronene
byte neuronVariables[numberOfNeurons][numberOfVariables];
>>>>>>> master

void setup(){
  for (int i = 0; i < numberOfNeurons; ++i) {
    neuronStatus[i] = 1;
  }
  Serial.begin(115200);
  Wire.begin(); // Addr skal være i argument. Blank = Master
  pinMode(button_pin, INPUT);
  pinMode(pin_1E, OUTPUT);
  pinMode(pin_1I, OUTPUT);
  pinMode(pin_2E, OUTPUT);
  pinMode(pin_2I, OUTPUT);
  pinMode(pin_3E, OUTPUT);
  pinMode(pin_3I, OUTPUT);
  digitalWrite(pin_1E, LOW);
  digitalWrite(pin_1I, LOW);
  digitalWrite(pin_2E, LOW);
  digitalWrite(pin_2I, LOW);
  digitalWrite(pin_3E, LOW);
  digitalWrite(pin_3I, LOW);
}

<<<<<<< HEAD
void loop(){ // Testkode
  if(!digitalRead(button_pin)){
    int time1 = micros();
    pingAllNeurons();
    int time2 = micros();
    Serial.println(time2-time1);
    printNeuronStatus();
    printAllVar(2);
    delay(300);
    testAllInputs();
  }

  if(Serial.available()){
    if( a % 2){
      updateVariabel(2, 0, 50);
      updateVariabel(2, 1, 25);
      updateVariabel(2, 2, 18);
      updateVariabel(2, 3, 23);
      updateVariabel(2, 4, 23);
      updateVariabel(2, 5, 23);
      updateVariabel(2, 6, 100);
      updateVariabel(2, 7, 100);
      updateVariabel(2, 8, 100);
      updateVariabel(2, 9, 20);
      updateVariabel(2, 10, 0);
      updateVariabel(2, 11, 10);
    }else{
      updateVariabel(2, 0, 50);
      updateVariabel(2, 1, 25);
      updateVariabel(2, 2, 18);
      updateVariabel(2, 3, 23);
      updateVariabel(2, 4, 23);
      updateVariabel(2, 5, 23);
      updateVariabel(2, 6, 10);
      updateVariabel(2, 7, 10);
      updateVariabel(2, 8, 10);
      updateVariabel(2, 9, 0);
      updateVariabel(2, 10, 2);
      updateVariabel(2, 11, 1);
      }
    ++a;
    while(Serial.available()){Serial.read();}
  } 
=======
void loop() {
  int time1 = micros();
  pingAllNeurons();
  int time2 = micros();
  Serial.println(time2-time1);
  printNeuronStatus();
  printAllVar(4);
  printAllVar(5);
  delay(100);
  if (Serial.available()) {
    updateVariabel(5, 3, 120);
    while (Serial.available()) {
      Serial.read();
    }
  }
  delay(1000);
>>>>>>> master
}

// Denne funksjonen oppdaterer tilkoblingsstatus for alle nevronene
// Om den detekterer en statusendring vil funksjonen oppdatere alle variablene til nevronet

<<<<<<< HEAD
void testAllInputs(){
 /*
  digitalWrite(pin_1E, HIGH);
  delay(112);
  digitalWrite(pin_1E, LOW);
  delay(1000);
  */
  digitalWrite(pin_1I, HIGH);
  delay(112);
  digitalWrite(pin_1I, LOW);
  delay(1000);
/*
  digitalWrite(pin_2E, HIGH);
  delay(112);
  digitalWrite(pin_2E, LOW);
  delay(1000);
  
  digitalWrite(pin_2I, HIGH);
  delay(112);
  digitalWrite(pin_2I, LOW);
  delay(1000);
  
  digitalWrite(pin_3E, HIGH);
  delay(112);
  digitalWrite(pin_3E, LOW);
  delay(1000);

  digitalWrite(pin_3I, HIGH);
  delay(112);
  digitalWrite(pin_3I, LOW);
  delay(1000);
*/
  
  
}

void pingAllNeurons(){
  for(int i = 1; i <= numberOfNeurons; i++){ // Nevronene har adresse 1-20
    Wire.beginTransmission(i);
    bool newResp = Wire.endTransmission();
=======
void updateSingleNeuronStatus(byte neuronAddress) {
  // Setter status-flagget til nevronet basert på om det svarer på ping
  setNeuronStatus(neuronAddress, pingSingleNeuron(neuronAddress) == 0);
}

void updateAllNeuronStatuses() {
  // Går gjennom alle tilkoblede nevroner og oppdaterer status-flaggene deres
  for (int i=1; i<=numberOfNeurons; ++i) {
    updateSingleNeuronStatus(i);
  }
}

// Oppdater alle variablene til ett gitt nevron
bool requestAllVariables(int neuronAddress){
  if (sendInstruction(neuronAddress, 100) == 0) {  // Connection still good
    byte variables[numberOfVariables]; // Midlertidig liste med alle variablene til valgte nevron
    bool neuronIsSynced = true; // Midlertidig flagg som indikerer om nevronet har samme variabelverdier som i lista på hub
>>>>>>> master
    
    int numberOfBytesRecieved = Wire.requestFrom(neuronAddress, numberOfVariables); // Ber om data og lagrer bytes som blir sendt tilbake
    
    if (numberOfBytesRecieved == numberOfVariables) { // Går videre om riktig antall variabler blir sendt tilbake
      for(int i = 0; i < numberOfVariablesRecieved; ++i){
        variables[i] = Wire.read();
        if (variables[i] != getNeuronVariable(neuronAddress, i) {
          neuronIsSynced = false;
        }
      }
    }
    return true; // Indikerer at alt gikk bra
  } else { 
    return false; // Lost connection
  } 
}

<<<<<<< HEAD
// Oppdater alle variablene til ett gitt nevron
bool requestAllVariables(int addr){
  Wire.beginTransmission(addr);
  bool stat = Wire.endTransmission();
  delay(10);
  if(!stat){  // Connection still good
    Wire.requestFrom(addr, numberOfVariables); // Ber om data
    for(int i = 0; i < numberOfVariables; i++){
      byte var = Wire.read();
      neuron_Variables[addr-1][i] = var;
    }
    return 0; // 0 Indikerer at alt gikk bra
  }else{return stat;} // Lost connection
=======
bool sendInstruction(byte neuronAddress, byte command, byte parameter) {
  Wire.beginTransmission(neuronAddress);
  Wire.write(command);
  Wire.write(parameter);
  return (Wire.endTransmission == 0);
}

bool sendInstruction(byte neuronAddress, byte command) {
  return sendInstruction(neuronAddress, command, 0);
>>>>>>> master
}

// Oppdater status for ett enkelt nevron
byte pingSingleNeruon(int neuronAddress){
    Wire.beginTransmission(neuronAddress);
    return Wire.endTransmission();
}

// Funskjonsnavn sier alt
void printAllVar(int addr){
  for(int j = 0; j < numberOfVariables; j++){
    Serial.print(neuron_Variables[addr-1][j], DEC);
    Serial.print(", ");
  }
  Serial.println("");
}

// Funskjonsnavn sier alt
void printNeuronStatuses(){
  for(int i = 0; i < numberOfNeurons; i++){
    Serial.print(getNeuronStatus(i), DEC);
    Serial.print(", ");
  }
  Serial.println("");
}

// Oppdaterer en gitt variabel hos ett gitt nevron med listen over variabler på huben
bool updateSingleNeuronVariable(byte neuronAddress, byte variableAddress){
  return sendInstruction(neuronAddress, variableAddress, getNeuronVariable(neuronAddress, variableAddress));
}

// Returnerer status (om nevronet er tilgjengelig eller ikke) til et gitt nevron
bool getNeuronStatus(byte neuronAddress) {
  return neuronStatus[neuronAddress - 1];
}

// Setter statusen til et gitt nevron (true = tilgjengelig, false = utilgjengelig)
void setNeuronStatus(byte neuronAddress, bool newStatus) {
  neuronStatus[neuronAddress - 1] = newStatus;
}

// Returnerer en gitt variabel fra variabellisten til et gitt nevron
byte getNeuronVariable(byte neuronAddress, byte variableAddress) {
  return neuronVariables[neuronAddress - 1][variableAddress];
}

// Setter en variabel i variabellisten til et gitt nevron
void setNeuronVariable(byte neuronAddress, byte variableAddress, byte newValue) {
  neuronVariables[neuronAddress - 1][variableAddress] = newValue;
}

