class Variable {
  String name, unit;
  int index, value, minValue, maxValue;
  boolean global, isZero;
  float mapMin, mapMax;
  
  Variable(int iIndex, String iName, String iUnit, int iValue, int iMinValue, int iMaxValue, float iMapMin, float iMapMax, boolean iglobal) {
    index = iIndex;
    name = iName;
    unit = iUnit;
    value = iValue;
    minValue = iMinValue;
    maxValue = iMaxValue;
    mapMin = iMapMin;
    mapMax = iMapMax;
    isZero = false;
    global = iglobal;
  }
  
  Variable(int iIndex, String iName, String iUnit, int iMinValue, int iMaxValue, float iMapMin, float iMapMax, boolean iglobal) {
    index = iIndex;
    name = iName;
    unit = iUnit;
    minValue = iMinValue;
    maxValue = iMaxValue;
    value = minValue;
    mapMin = iMapMin;
    mapMax = iMapMax;
    isZero = false;
    global = iglobal;
  }
  
  int getValue() {
    return value;
  }

  float getNormalizedValue() {
    return 1.0 * (value - minValue) / (maxValue - minValue);
  }
  
  String getCorrectValue() {
      return nf(map(value, minValue, maxValue, mapMin, mapMax), 0, 1) + unit;
  }
  
  void setValue(int newValue) {
    value = newValue;
  }

  void setValue(float normal) {
    // Setter verdien basert på en float
    // 0 -> minValue
    // 1 -> maxValue
    value = round(constrain(normal, 0, 1) * (maxValue - minValue) + minValue);
  }

  boolean incrementValue() {
    if (value < maxValue) {
      ++value;
      return true;
    } else {
      return false;
    }
  }

  boolean decrementValue() {
    if (value > minValue) {
      --value;
      return true;
    } else {
      return false;
    }
  }

  int getMaxValue() {
    return maxValue;
  }
  
  int getMinValue() {
    return minValue;
  }

  int getIndex() {
    return index;
  }

  String getName() {
    return name;
  }

  String getUnit() {
    return unit;
  }
  
  boolean toggleZero() {
    isZero = !isZero;
    return isZero;
  }
  
  boolean isZero() {
    return isZero;
  }
  
  boolean isGlobal() {
    return global;
  }

  void printInfo() {
    println(name + ": " + value);
  }
}