class Neuron {
  int id;
  
  ArrayList<Variable> variables;
  
  Neuron(int iId) {
    id = iId;
    variables = new ArrayList<Variable>();

    variables.add(new Variable(0, "Time constant", "ms", 1, 150, 1, 150, false));
    variables.add(new Variable(1, "Threshold potential", "mV", 5, 35, 5, 35, false));
    variables.add(new Variable(2, "Afterhyperpolarization", "mV", 0, 39, -80, -41, false));
    
    variables.add(new Variable(3, "Dendrite 1 weight", "mV", 1, 50, 1, 50, false));
    variables.add(new Variable(4, "Dendrite 2 weight", "mV", 1, 50, 1, 50, false));
    variables.add(new Variable(5, "Dendrite 3 weight", "mV", 1, 50, 1, 50, false));
    
    variables.add(new Variable(6, "Dendrite 1 delay", "ms", 0, 100, 0, 1000, false));
    variables.add(new Variable(7, "Dendrite 2 delay", "ms", 0, 100, 0, 1000, false));
    variables.add(new Variable(8, "Dendrite 3 delay", "ms", 0, 100, 0, 1000, false));
    
    variables.add(new Variable(9, "Spontaneous activity", "Hz", 0, 100, 0, 50, false));
    variables.add(new Variable(10, "Light sensor", "Hz", 0, 100, 0, 100, false));
    
    variables.add(new Variable(11, "Slow motion", "x", 1, 20, 1, 20, true));
  }
  
  boolean getVariables(Serial serialPort) {
    if (id != 0) {
      // Henter variabler som er lagret i nevron
      int[] newVariables = sendInstruction(serialPort, 100, id, 0, variables.size());
      
      // Går bare videre hvis kommunikasjon med nevronet fungerer
      if (newVariables[0] == 0) { 
        // Oppdaterer variabler lagret her       
        for (Variable variable : variables) {
          variable.setValue(newVariables[variable.getIndex() + 1]);
        }
        return true;
      }
    }
    return false;
  }

  void getVariables(Neuron sourceNeuron) {
    for (int i=0; i<variables.size(); ++i) {
      variables.get(i).setValue(sourceNeuron.variables.get(i).getValue());
    }
  }

  boolean setVariables(Serial serialPort) {
    for (int i=0; i<variables.size(); ++i) {
      if (sendInstruction(serialPort, i, id, variables.get(i).getValue())[0] != 0) {
         return false; 
      }
    }
    return true;    
  }
  
  void printInfo() {
    println("\n--- Neuron " + id + " ---");
    for (Variable variable : variables) {
      variable.printInfo();
    }
  }
  
  void printExternalVariables(Serial serialPort) {
    if (id != 0) {
      int[] externalVariables = sendInstruction(serialPort, 100, id, 0, variables.size());
      if (externalVariables[0] == 0) {
        println("\n-- External variables of neuron " + id + " --");
        for (int i=0; i<variables.size(); ++i) {
          println(variables.get(i).getName() + ": " + externalVariables[i + 1]);
        }
      }
    }  
  }
  
  int getId() {
    return id;
  }
  
  String getInfo() {
    String string = "Neuron " + id + "\n";
    for (int i=0; i<variables.size(); ++i) {
      string += "\n" + variables.get(i).getName() + ": " + variables.get(i).getCorrectValue();
    }
    return string;
  } 
}