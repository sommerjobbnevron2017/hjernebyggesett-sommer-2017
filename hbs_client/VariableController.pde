class VariableController {
  Variable slaveVariable;
  int x, y, w, h;
  
  VariableController(Variable islaveVariable, int ix, int iy, int iw, int ih) {
    slaveVariable = islaveVariable;
    x = ix;
    y = iy;
    w = iw;
    h = ih;
  }
  void draw() {};
}

class Slider extends VariableController{
  // Estetisk
  int hw;
  
  // Teknisk
  boolean beingDragged;

  Slider(Variable islaveVariable, int ix, int iy, int iw, int ih, int ihw) {
    super(islaveVariable, ix, iy, iw, ih);
    
    hw = ihw;
    
    beingDragged = false;
  }
  
  void draw() {
    if (beingDragged) {
      if (mousePressed) {
        slaveVariable.setValue((mouseX - x - hw * 1.5) / (w - 3 * hw));
      } else {
        beingDragged = false;
      }  
    }
    // Minusknapp
    if (drawButton(x, y, hw, h, "-", darkGray, lightestGray)) {
      slaveVariable.decrementValue();
    }
    
    // Plussknapp
    if (drawButton(x + w - hw, y, hw, h, "+", darkGray, lightestGray))  {
      slaveVariable.incrementValue();
    }

    stroke(darkGray);
    line(x + hw, y + h / 2, x + w - hw, y + h / 2);
  
    if (drawButton(x + hw + round(slaveVariable.getNormalizedValue() * (w - 3 * hw)), y, hw, h, "", darkGray, lightestGray)) {
      beingDragged = true;
    }
  }
}

class Toggle extends VariableController {
  int lastValue;
  
  Toggle(Variable islaveVariable, int ix, int iy, int iw, int ih) {
    super(islaveVariable, ix, iy, iw, ih);
    
    lastValue = 0;
  }
  
  void draw() {
    if (drawButton(x, y, w, h, (slaveVariable.isZero())?"OFF":"ON", darkGray, lightestGray)) {
      if (slaveVariable.isZero()) {
        if (lastValue == 0) {
          slaveVariable.setValue(slaveVariable.getMaxValue());
        } else {
          slaveVariable.setValue(lastValue);
        }
      } else {
        lastValue = slaveVariable.getValue();
        slaveVariable.setValue(0);
      }
      slaveVariable.toggleZero();
    }
  }
}

class Display extends VariableController {
  boolean hasSlider, hasToggle;
  VariableController slider, toggle;
  
  Display(Variable islaveVariable, int ix, int iy, int iw, boolean ihasSlider, boolean ihasToggle) {
    super(islaveVariable, ix, iy, iw, 20);
    hasSlider = ihasSlider;
    hasToggle = ihasToggle;
    

    if (hasToggle) {
      toggle = new Toggle(slaveVariable, x + 220, y, 60, h);
      if (hasSlider) {
        slider = new Slider(slaveVariable, x + 290, y, w - 290, h, h);
      } else {
        slider = null;
      }
    } else {
      toggle = null;
      if (hasSlider) {
        slider = new Slider(slaveVariable, x + 220, y, w - 220, h, h);
      } else {
        slider = null;
      }
    }
  }
  
  void draw() {
    textFont(smallFont);
    fill(darkestGray);
    
    textAlign(RIGHT, TOP);
    text(slaveVariable.getName() + ":", x + 140, y + (h - 12) / 2);
    
    textAlign(LEFT, TOP);
    text(slaveVariable.getCorrectValue(), x + 150, y + (h - 12) / 2);
    
    if (hasToggle) {
      toggle.draw();
    }
    
    if (hasSlider) {
      slider.draw();
    }
  }
}