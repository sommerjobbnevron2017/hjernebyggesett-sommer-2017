import processing.serial.*;
Serial port;

final String appVersion = "1.0.3";

String view = "select_port";
PFont smallFont, mediumFont;
final color darkestGray = #333333;
final color darkGray = #666666;
final color gray = #999999;
final color lightGray = #CCCCCC;
final color lightestGray = #FFFFFF;

long messageTimer = -5000;
String messageText = "";

final int maxNeurons = 25;
ArrayList<Neuron> neurons;
ArrayList<Neuron> selectedNeurons;
Neuron selectedNeuron;

ArrayList<VariableController> variableControllers;

boolean pmousePressed = false;

void setup() {
  size(960, 720);
  
  smallFont = loadFont("Verdana-12.vlw");
  mediumFont = loadFont("CourierNewPSMT-20.vlw");
  textFont(smallFont);
  textAlign(LEFT, TOP);
  
  neurons = new ArrayList<Neuron>();

  selectedNeurons = new ArrayList<Neuron>();
  selectedNeuron = new Neuron(0);
  
  variableControllers = new ArrayList<VariableController>();
  int i = 70;
  variableControllers.add(new Display(selectedNeuron.variables.get(0), 240, i, 700, true, false));
  i += 50;
  
  variableControllers.add(new Display(selectedNeuron.variables.get(1), 240, i, 700, true, false));
  i += 50;
  
  variableControllers.add(new Display(selectedNeuron.variables.get(2), 240, i, 700, true, false));
  i += 50;

  variableControllers.add(new Display(selectedNeuron.variables.get(3), 240, i, 700, true, false));
  i += 35;
  
  variableControllers.add(new Display(selectedNeuron.variables.get(4), 240, i, 700, true, false));
  i += 35;
  
  variableControllers.add(new Display(selectedNeuron.variables.get(5), 240, i, 700, true, false));
  i += 50;
  
  variableControllers.add(new Display(selectedNeuron.variables.get(6), 240, i, 700, true, false));
  i += 35;
  
  variableControllers.add(new Display(selectedNeuron.variables.get(7), 240, i, 700, true, false));
  i += 35;
  
  variableControllers.add(new Display(selectedNeuron.variables.get(8), 240, i, 700, true, false));
  i += 50;
  
  variableControllers.add(new Display(selectedNeuron.variables.get(9), 240, i, 700, true, false));
  i += 50;
  
  variableControllers.add(new Display(selectedNeuron.variables.get(10), 240, i, 700, true, false));
  i += 50;
  
  variableControllers.add(new Display(selectedNeuron.variables.get(11), 240, i, 700, true, false));
  
}

void draw() {
  background(lightestGray);
  switch (view) {
    case "select_port":
      String[] ports = Serial.list();
      
      textFont(mediumFont);
      textAlign(RIGHT, TOP);
      fill(darkestGray);
      text("Choose the hub port", width/2 - 20, height / 2 - 20);
      fill(gray);
      text("Unplug the USB cable and plug it\nback in if you are uncertain\nof the port.", width / 2 - 20, height / 2);
      
      stroke(gray);
      line(width / 2, height / 2 - ports.length * 30 + 5, width / 2, height / 2 + ports.length * 30 - 5);
      line (width / 2, height / 2 - ports.length  * 30 + 5, width / 2 + 5, height / 2 - ports.length * 30 + 5);
      line (width / 2, height / 2 + ports.length * 30 - 5, width / 2 + 5, height / 2 + ports.length * 30 - 5);
      line (width / 2 - 5, height / 2, width / 2 , height / 2);
      
      // Tegner port-liste
      for (int i=0; i<ports.length; ++i) {
        if (drawButton(width / 2 + 20, height / 2 - ports.length * 30 + 5 + i * 60, width / 2 - 40, 50, ports[i], darkGray, lightestGray)) {
          port = new Serial(this, ports[i], 9600);
          delay(1500);
          
          updateConnectedNeurons(port);
          
          view = "monitor_neurons";
          break;
        }
      }
      break;
    case "monitor_neurons":    
      noStroke();
      fill(lightGray);
      rect(0, 50, 220, height - 50);
      for (int i=0; i<neurons.size(); ++i) {
        if (neuronIsSelected(neurons.get(i)) != -1) {
          if (drawButton(10, 60 + i * 24, 210, 20, "Neuron " + str(neurons.get(i).getId()), darkGray, lightestGray)) {
            toggleNeuronSelection(neurons.get(i));
          }
        } else {
          if (drawButton(10, 60 + i * 24, 200, 20, "Neuron " + str(neurons.get(i).getId()), darkestGray, lightGray)) {
            toggleNeuronSelection(neurons.get(i));
          }
        }
      }
      stroke(darkestGray);
      line(220, 50, 220, height);
      
      if (selectedNeurons.size() > 0) { 
        if (selectedNeurons.size() == 1) {
          for (int i=0; i<variableControllers.size(); ++i) {
            variableControllers.get(i).draw(/*selectedNeurons.get(0).variables.get(i).getCorrectValue()*/);
          }
        } else {
          for (VariableController variableController : variableControllers) {
            variableController.draw();
          }
        }

        if (drawButton(230, height - 40, 720, 30, "Upload to neurons: " + getSelectedNeurons(), #44aa44, lightestGray)) {
          uploadVariablesToNeurons(port);          
        }
      } else {
        textAlign(LEFT, TOP);
        textFont(mediumFont);
        fill(gray);
        text("<- Click neurons from the list to the left to select/unselect them. When one or more neurons are selected you can adjust their values.", 250, 80, 400, 300);
      }
      noStroke();
      fill(lightestGray);
      rect(0, 0, width, 50);
      stroke(darkestGray);
      line(0, 50, width, 50);
      fill(darkestGray);
      textAlign(LEFT, TOP);
      textFont(mediumFont);
      text("NeuronController " + appVersion, 20, 20);
      
      if (drawButton(width - 300, 10, 140, 30, "Refresh", darkGray, lightestGray)) {
        updateConnectedNeurons(port);
      }
      
      if (drawButton(width - 150, 10, 140, 30, "Disconnect", #AA3333, lightestGray)) {
        port.stop();
        neurons.clear();
        selectedNeurons.clear();
        view = "select_port";
      }
      break;
  }
  
  // Tegner melding
  if (millis() - messageTimer < 2500) {
    stroke(darkestGray);
    fill(lightestGray);
    rect(width / 2 - 200, height / 2 - 100, 400, 200);
    textFont(mediumFont);
    textAlign(CENTER, CENTER);
    fill(darkestGray);
    text(messageText, width / 2 - 180, height / 2 - 90, 360, 180);
  }
  
  pmousePressed = mousePressed;
}


boolean drawButton(int x, int y, int w, int h, String text, color strongColor, color weakColor) {
  return drawButton(x, y, w, h, text, weakColor, strongColor, strongColor, strongColor, strongColor, weakColor);
}

boolean drawButton(int x, int y, int w, int h, String text, color bagrColor, color brdrColor, color textColor, color hovrBagrColor, color hovrBrdrColor, color hovrTextColor) {
  if (mouseX > x && mouseX <= x + w && mouseY > y && mouseY <= y + h) {
    fill(hovrBagrColor);
    stroke(hovrBrdrColor);
    rect(x, y, w, h);
    fill(hovrTextColor);
    textFont(smallFont);
    textAlign(CENTER, CENTER);
    text(text, x + w / 2, y + h / 2);
  
    if (mousePressed && !pmousePressed) {
      return true;
    }
  } else {
    fill(bagrColor);
    stroke(brdrColor);
    rect(x, y, w, h);
    fill(textColor);
    textFont(smallFont);
    textAlign(CENTER, CENTER);
    text(text, x + w / 2, y + h / 2);
  }
  return false;
}

boolean addNeuron(int id) {
  for (Neuron n : neurons) {
    if (n.id == id) {
      return false;
    }
  }
  neurons.add(new Neuron(id));
  return true;
}

boolean removeNeuron(int id) {
  for (int i=0; i<neurons.size(); ++i) {
    if (neurons.get(i).id == id) {
      neurons.remove(i);
      return true;
    }
  }
  return false;
}

int neuronIsSelected(Neuron neuron) {
  for (int i=0; i<selectedNeurons.size(); ++i) {
    if (selectedNeurons.get(i) == neuron) {
      return i;
    }
  }
  return -1;
}

boolean toggleNeuronSelection(Neuron neuron) {
  
  // Sjekker om nevronet allerede ligger i lista over valgte nevroner
  int isSelected = neuronIsSelected(neuron);
  
  if (isSelected != -1) { // Nevronet er allerede valgt
     
    // Nevronet fjernes fra lista
    selectedNeurons.remove(isSelected);
    
    return false;
    
  } else { // Nevronet er ikke valgt
    
    // Hvis lista er tom får selectedNeuron samme verdier
    if (selectedNeurons.size() == 0) {
      selectedNeuron.getVariables(neuron);
      
      // Litt hardkoding for å få lyssensorknappen til å blir helt strømlinjeformet
      if (selectedNeuron.variables.get(9).getValue() == 0) {
        selectedNeuron.variables.get(9).toggleZero();
      }
      // Tilsvarende for spontantfyring
      if (selectedNeuron.variables.get(10).getValue() == 0) {
        selectedNeuron.variables.get(10).toggleZero();
      }
    }
    
    // Nevronet blir lagt til i lista 
    selectedNeurons.add(neuron);
    
    return true;
  }
}

int receiveByte(Serial serialPort, int timeout) {
  int pause = 20;
  for (int i=0; i<timeout / pause; ++i) {
    if (serialPort.available() > 0) {
      return serialPort.read();
    }
    delay(pause);
  }
  return -1;
}

int[] sendInstruction(Serial serialPort, int command, int id, int parameter, int returnedValues) {
  int[] values = new int[returnedValues + 1];
  serialPort.write(byte(command));
  serialPort.write(byte(id));
  serialPort.write(byte(parameter));
  serialPort.write(byte(returnedValues));
  values[0] = receiveByte(serialPort, 100);
  if (values[0] == 0) { // Leser bare av resten av dataen hvis nevronet svarer huben
    for (int i=1; i<=returnedValues; ++i) {
      values[i] = receiveByte(serialPort, 100);
    }
  }
  delay(10); // Legger inn et lite delay for at ikke instruksjonene skal komme for tett
  return values;
}

int[] sendInstruction(Serial serialPort, int command, int id, int parameter) {
  return sendInstruction(serialPort, command, id, parameter, 0);
}

int[] sendInstruction(Serial serialPort, int command, int id) {
  return sendInstruction(serialPort, command, id, 0 , 0);
}

void updateConnectedNeurons(Serial serialPort) {
  neurons.clear();
  selectedNeurons.clear();
  for (int j=1; j<=maxNeurons; ++j) {
    if (sendInstruction(serialPort, 255, j)[0] == 0) { // Pinger alle nevroner, og legger de som svarer til i nevron-lista
      addNeuron(j);
    }
  }
  
  for (Neuron neuron : neurons) {
    neuron.getVariables(port); // Laster inn variablene fra tilsvarende fysiske nevron
    neuron.printInfo();
  }
}

boolean uploadVariablesToNeurons(Serial serialPort) {
  boolean result = true;
  for (Neuron neuron : selectedNeurons) {
    // Henter inn nye variabelverdier fra selectedNeuron
    neuron.getVariables(selectedNeuron);
    
    // Laster de nye variablene opp til fysiske nevroner
    if (!neuron.setVariables(serialPort)) {
      result = false;
      break;
    }
    
    // Skriver ut variablene til det eksterne nevronet
    neuron.printExternalVariables(port);
  }
  
  if (result) {
    showMessage("Upload successfull!");
  } else {
    showMessage("Upload UNSUCCESSFULL!");
  }

  return true;
}

boolean stringInStringArray(String string, String[] stringArray) {
  for (int i=0; i<stringArray.length; ++i) {
    if (string.equals(stringArray[i])) {
      return true;
    }
  }
  return false;
}

String getSelectedNeurons() {
  String string = "";
  for (int i=0; i<selectedNeurons.size(); ++i) {
    if (i != 0) {
      string += ", ";
    }
    string += selectedNeurons.get(i).getId();
  }
  return string;
}

void showMessage(String message) {
  messageText = message;
  messageTimer = millis();
}
