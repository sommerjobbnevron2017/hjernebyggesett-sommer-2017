#include <Wire.h> // I2C bibliotek
// For Arduino Micro
// SDA (Data line)  = Dig. Pin# 2
// SCL (Clock)      = Dig. Pin# 3

// Konstanter
const int numberOfNeurons = 20; // Maks antall nevron i nettverket
const int numberOfVariables = 10;// Antall variabler pr nevron

// Liste over tilkoblingsstatus på nevronene. 0 Indikerer tilkoblet. != 0 Frakoblet/ukjent
// Indeks samsvarer med addressen - 1. På adresse 0 svarer alle slavene, derfor uinteressant
// å lagre informasjon på denne adressen.
bool neuronStatus[numberOfNeurons];
// Dobbelt liste over alle variablene til nevronene
byte neuron_Variables[numberOfNeurons][numberOfVariables];



void setup(){
  for(int i = 0; i < numberOfNeurons; i++){neuronStatus[i] = 1;}
  Serial.begin(115200); // Midlertig.
  Wire.begin(); // Addr skal være i argument. Blank = Master
}

void loop(){ // Testkode
  int time1 = micros();
  pingAllNeurons();
  int time2 = micros();
  Serial.println(time2-time1);
  printNeuronStatus();
  printAllVar(4);
  printAllVar(5);
  delay(100);
  if(Serial.available()){
    updateVariabel(5, 3, 120);
    while(Serial.available()){Serial.read();}
  }
  delay(1000);
  
}

// Denne funksjonen oppdaterer tilkoblingsstatus for alle nevronene
// Om den detekterer en statusendring vil funksjonen oppdatere alle variablene til nevronet
void pingAllNeurons(){
  for(int i = 1; i <= numberOfNeurons; i++){
    Wire.beginTransmission(i);
    bool newResp = Wire.endTransmission();
    
    if((newResp == 0) && (neuronStatus[i-1] != 0)){ // Neuron connected
      Serial.print("Neuron connected: ");
      Serial.println(i);
      neuronStatus[i-1] = requestAllVariables(i);   // Oppdater variabler
    }
    
    else if( (newResp != 0) && (neuronStatus[i-1] == 0)){ // Nevron disconnected
      Serial.print("Neuron disconnected: ");
      Serial.println(i);
      for(int j = 0; j < numberOfVariables; j++){ // Nuller alle variabler
        neuron_Variables[i-1][j] = 0;
      }
      neuronStatus[i-1] = newResp;
    }
    
    else{neuronStatus[i-1] = newResp;} // Oppdater status
  }
}

// Oppdater alle variablene til ett gitt nevron
bool requestAllVariables(int addr){
  Wire.beginTransmission(addr); // Begynner med å oppdatere kommando
  Wire.write(100); // Slave forventer alltid to bytes hver gang
  Wire.write(100);
  bool stat = Wire.endTransmission();
  if(!stat){  // Connection still good
    Wire.requestFrom(addr, numberOfVariables); // Ber om data
    for(int i = 0; i < numberOfVariables; i++){
      byte var = Wire.read();
      neuron_Variables[addr-1][i] = var;
    }
    return 0; // 0 Indikerer at alt gikk bra
  }else{return stat;} // Lost connection
}

// Oppdater status for ett enkelt nevron
void pingSingleNeruon(int i){
    Wire.beginTransmission(i);
    bool newResp = Wire.endTransmission();
    
    if((newResp == 0) && (neuronStatus[i-1] != 0)){ // Neuron connected
      Serial.print("Neuron connected: ");
      Serial.println(i);
      neuronStatus[i-1] = requestAllVariables(i);   // Oppdater variabler
    }
    
    else if( (newResp != 0) && (neuronStatus[i-1] == 0)){ // Nevron disconnected
      Serial.print("Neuron disconnected: ");
      Serial.println(i);
      for(int j = 0; j < numberOfVariables; j++){ // Nuller alle variabler
        neuron_Variables[i-1][j] = 0;
      }
      neuronStatus[i-1] = newResp;
    }
    
    else{neuronStatus[i-1] = newResp;} // Oppdater status
}

// Funskjonsnavn sier alt
void printAllVar(int addr){
  for(int j = 0; j < numberOfVariables; j++){
    Serial.print(neuron_Variables[addr-1][j], DEC);
    Serial.print(", ");
  }Serial.println("");
}

// Funskjonsnavn sier alt
void printNeuronStatus(){
  for(int i = 0; i < numberOfNeurons; i++){
    Serial.print(neuronStatus[i], DEC);
    Serial.print(", ");
  }Serial.println("");
}

// Oppdaterer en gitt variabel til ett gitt nevron
bool updateVariabel(int addr, int varNum, int varVal){
  // Starter kommunikasjon med nevron
  Wire.beginTransmission(addr);
  Wire.write(varNum); // Første byte tilsvarer variabelnr/index
  Wire.write(varVal); // Neste byte er ny verdi
  bool stat = Wire.endTransmission(); // Avslutter overføring
  // Hvis overføringen var vellykket oppdateres varibler hos master
  // siden det foregår mye på nevronet samtidig må en vente litt med 
  // request rett etter en transmission
  delay(10);
  if(!stat){requestAllVariables(addr);} 
  return stat; // Returverdi sier om oppdateringen var vellykket eller ikke. 0 = vellykket
}

