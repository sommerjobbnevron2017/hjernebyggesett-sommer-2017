#include <Wire.h>
#include <EEPROM.h>

const int numberOfVariables = 6;
byte command = 0; // Kommando definerer hva som skjer ved master request.

// Alle variabler må lagres i en liste
byte variables[numberOfVariables] = {70,66,69,90,91,93}; 
//#1 Tidskonstant
//#2 Propageringskonstant
//#3 Terskelspenning
//#4 Vekt_dendritt1
//#5 Vekt_dendritt2
//#6 Vekt_dendritt3

void setup() {
  //Serial.begin(115200);
  Wire.begin(14);               // join i2c bus with address
  Wire.onReceive(receiveEvent); // register event
  Wire.onRequest(requestEvent); // register event

}

void loop() {
  // put your main code here, to run repeatedly:
  delay(100);
}


void receiveEvent(int howMany){ // Mottar bytes

  // Sjekker om master PINGer eller sender kommando
  if (Wire.available() > 0) {
    // Kommando sendt
    command = Wire.read(); // Første byte er kommando. Hva skal gjøres.
    byte val = Wire.read();
    if(command == 100){
      break; // Variabel request. Håndteres i egen funskjon
    }else if( (0 <= command) && (command < 6) ) {
      variables[command] = val;
    }
  }
  
  // commando 0 = Send alle variabler. Dette er en request event og håndteres der.
  
  // commando 1 = Endre variabel. Ny variabel skal skrives over
  
  
  // Tømmer bufferet i tilfelle det er bytes det ikke er tatt høyde for på busen.
  while(Wire.available()){Wire.read();} 

}

void requestEvent(){
  switch(command){
    case 0: // Send alle variabler
      Wire.write(variables, numberOfVariables);
      break;
    case 1: // Oppdater variabel. Håndteres i receive
      break;
    case 2: // Ledig
      break;   

  }
}




