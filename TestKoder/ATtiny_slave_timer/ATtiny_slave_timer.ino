/*
http://thewanderingengineer.com/2014/02/17/attiny-i2c-slave/
*/
//#include <avr/io.h>

const int numberOfVariables = 6;
// Kommando definerer hva som skjer ved master request.
uint8_t command = 0; 

// Alle variabler må lagres i en liste
byte variables[numberOfVariables] = {70,66,69,90,91,93}; 
//#1 Tidskonstant
//#2 Propageringskonstant
//#3 Terskelspenning
//#4 Vekt_dendritt1
//#5 Vekt_dendritt2
//#6 Vekt_dendritt3


#define I2C_SLAVE_ADDRESS 0x4   //this slave address (0x1, 0x2, 0x3)
#include <TinyWireS.h>          //the ATTiny Wire library
int LEDPin = A0;                //indicator LED
int counter1;
int counter2;

void setup() {
  cli();
  // Kobler seg på bussen som slave
  TinyWireS.begin(I2C_SLAVE_ADDRESS); 
  // Argumentet sier hvilket funskjon som kalles når slaven blir addressert på busen
  TinyWireS.onReceive(receiveEvent);
  TinyWireS.onRequest(requestEvent);
  pinMode(LEDPin, OUTPUT);
  TCCR0A = 0; // Timer control register normal operation
  TCCR0A |= (1 << WGM01); // CTC-mode
  TCCR0B = 0b00000100;  // Pre-Scaler = 256
  OCR0A = 32; // Compare register A
  OCR0B = 8; // Compare register B
  TIMSK0 = 0b00000110;
  sei();

}

int i = 0;

void loop() {
  TinyWireS_stop_check(); // Foreløpig mystisk, men nødvendig
}

void requestEvent(){
  
  if(command == 100){
    // TinyWire støtter ikke sending av flere bytes i samme funksjon
    for(int i = 0; i < numberOfVariables; i++){
      TinyWireS.send(variables[i]);
    }
  } 
  command = -1; // Markerer utført commando
  
}


void receiveEvent(uint8_t howMany){
  
  // Sjekker om master PINGer eller sender kommando
  if(TinyWireS.available() > 0){
    // Kommando sendt
    command = TinyWireS.receive(); // Første byte er kommando. Hva skal gjøres.
    byte val = TinyWireS.receive();// evt verdi som skal settes
    if( (0 <= command) && (command < 6) ) {
      variables[command] = val;
    }    
  }
  while(TinyWireS.available()){TinyWireS.receive();}
 
}

ISR(TIM0_COMPA_vect){
  counter2++;
  if(counter2>=37){
    if(counter1%2){digitalWrite(LEDPin,HIGH);}
    else{digitalWrite(LEDPin, LOW);}
    counter1++;
    counter2= 0;
  } 
}

ISR(TIM0_COMPB_vect){
   
}

