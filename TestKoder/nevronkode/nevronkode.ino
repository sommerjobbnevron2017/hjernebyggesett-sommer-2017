// avr-libc library includes
#include <avr/io.h>
#include <TinyWireS.h>
//Arduino IDE & Pin Mapping 
// ATMEL ATTINY84 / ARDUINO
//
//                           +-\/-+
//                     VCC  1|    |14  GND
//             (D 10)  PB0  2|    |13  AREF (D  0)
//             (D  9)  PB1  3|    |12  PA1  (D  1) 
//             RESET   PB3  4|    |11  PA2  (D  2) 
//  PWM  INT0  (D  8)  PB2  5|    |10  PA3  (D  3) 
//  PWM        (D  7)  PA7  6|    |9   PA4  (D  4) 
//  PWM        (D  6)  PA6  7|    |8   PA5  (D  5)        PWM
//                           +----+

//IDE Attiny84 Physical Pin
//  0      PA0           13
//  1      PA1           12
//  2      PA2           11
//  3      PA3           10
//  4      PA4            9
//  5      PA5            8
//  6      PA6            7
//  7      PA7            6
//  8      PB2            5
//  9      PB1            3
// 10      PB0            2

//Inkluderer NeoPixel-biblioteket som styrer LEDs
#include <Adafruit_NeoPixel.h>

// Definerer i/o-pins
#define pin_led 3 // LED-pin
#define pin_sensor 2 // Lyssensor pin
#define pin_axon 5 //Akson-pin
#define pin_dendrite1E 10 // Dendrittpin 1 eksitorisk
#define pin_dendrite1I 9 // Dendrittpin 1 inhibitorisk
#define pin_dendrite2E 8 // Dendrittpin 2 eksitorisk
#define pin_dendrite2I 7 // Dendrittpin 2 inhibitorisk
#define pin_dendrite3E 1 // Dendrittpin 3 eksitorisk
#define pin_dendrite3I 0 // Dendrittpin 3 inhibitorisk

// Definerer konstanter for bruk med LEDs
#define num_dendrites 2 // Antall dendritter
#define num_leds 5 // Antall leds, teller ikke med dendritt-LEDs

// Definerer konstanter som har med membranpotensialet å gjøre
byte potential_threshold = 100;// Terskelpotensialet
byte potential_dendrite_increment = 80; // Hvor mye membranpotensialet skal stige når en puls treffer en eksitorisk dendritt
byte potential_dendrite_decrement = 80; // Hvor mye membranpotensialet skal synke når en puls treffer en inhibitorisk dendritt
double potential_stabilize_factor = 0.99; // Hvor fort potensialet skal konvergere mot hvilenivået 
double potential = 0; // Membranpotensiale



// Variabler som har med timing å gjøre
  // Variablene som begynner med dur_ er konstanter som har med varighet å gjøre (dur for duration) 
  // Variablene som begynner med ts_ er timestamps som brukes til timing
  // Alle tider er gitt i millisekunder


#define dur_pulse_light 700 // Hvor lenge lysdiodene skal lyse etter fyring

unsigned long int ts_pulse_fire = 0; // tidsstempel for siste fyring
unsigned long int interrupt_counter = 0;
byte dur_dendrite_delay = 255; // Tidsforsinkelse fra dendritten mottar et signal til det videreføres inn til cellekjernen



#define dur_dendrite_reset 5 // Minimum opphold mellom hver gang dendrittene kan aktiveres
// Variablene under holder styr på når dendrittene sist ble aktivert
unsigned long int ts_dendrite1E_activated = 0; 
unsigned long int ts_dendrite1I_activated = 0;
unsigned long int ts_dendrite2E_activated = 0;
unsigned long int ts_dendrite2I_activated = 0; 
unsigned long int ts_dendrite3I_activated = 0;
unsigned long int ts_dendrite3E_activated = 0;

// Om lyssensor er aktiv eller ikke
bool lightSensor = 0;
// Om nevronet skal fyre
bool fire = 0;

// Variabler som har med puls-listen å gjøre
  // Det er en tidsforsinkelse fra en puls treffer en dendritt til den blir registrert, dette er av pedagogiske årsaker
  // For at alle pulser skal registreres legges de til i en liste når de treffer dendritten, og fjernes når de registreres
  // Begge listene er volatile da de skal endres på under interrupt
#define pulse_list_length 10
// Pulslisten, den inneholder tidsstempler for alle pulser som dendritten har mottat, men som ikke har fyrt enda
volatile unsigned long int pulse_list[pulse_list_length]{}; 
volatile bool pulses_type[pulse_list_length]; // Listen som holder styr på om pulsene i pulslisten er inhibitoriske eller eksitoriske

// Initierer LEDs
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(num_leds + num_dendrites, pin_led, NEO_GRB + NEO_KHZ800);


// Ting relatert til lysdelen
// Antall millisekunder det skal samples
#define dur_calibration 5000
#define num_calibration_samples 64
#define threshold_factor 10 // Hvor mange prosent høyere terskelen skal være i forhold til gjennomsnittet fra kallibreringen
unsigned int light0_val, light1_val, light_threshold;


void setup() {
  cli();// Skrur av interrupts
  
  // Setter opp i/o pins
  pinMode(pin_dendrite1E, INPUT);
  pinMode(pin_dendrite1I, INPUT);
  pinMode(pin_dendrite2E, INPUT);
  pinMode(pin_dendrite2I, INPUT);
  pinMode(pin_dendrite3I, INPUT);
  pinMode(pin_dendrite3E, INPUT);
  pinMode(pin_led, OUTPUT);
  pinMode(pin_axon, OUTPUT);
  pinMode(pin_sensor, OUTPUT);
  
  // Skriver lav til alle output-pins fir å være sikker på at de oppfører seg som tiltenkt
  digitalWrite(pin_led, LOW);
  digitalWrite(pin_axon, LOW);

  // Skriver 0 til alle felter i puls-listen
  for (byte i=0; i<pulse_list_length; ++i) {
    pulse_list[i] = 0;
  }


  PCMSK0 = 0b10000011; // Skrur på interrupts på pin A0, A1 og A7 
  PCMSK1 = 0b00000111; // Skrur på interrupts på pin B0, B1 og B2  
  GIMSK  = 0b00110000; // Skrur på pin change interrupts

  // Timer interrupt A
  TCCR0A = 0; // Timer control register normal operation
  TCCR0A |= (1 << WGM01); // CTC-mode
  TCCR0B = 0b00000100;  // Pre-Scaler = 256
  OCR0A = 31; // Compare register A
  OCR0B = 8; // Compare register B
  TIMSK0 = 0b00000110;
  
  // Interrupt frekvens =  ( 8MHz / Prescaler ) / OCR0A 

  
  // Kalibrerer
//  bool calibrating = true;
//  
//  while (calibrating) {
//    // Midlertidige variabler som brukes til kalibrering
//    unsigned long int sample_sum = 0;
//    unsigned long int ts_sample;
//    for (int i=0; i<num_calibration_samples; ++i) {
//      sample_sum += analogRead(pin_sensor);
//      ts_sample = myMillis();
//      while (myMillis() - ts_sample < dur_calibration/num_calibration_samples) {
//        // Fin pulserende lyseffekt mens det kalibreres
//        for (byte i = 0; i < num_leds; ++i){
//          pixels.setPixelColor(i, pixels.Color(round(30*(sin(myMillis()/1000.0*PI+i*PI/num_leds)+1)), 0, 0));
//        }
//        pixels.show();
//      }
//    }
//    unsigned int average = round(sample_sum/num_calibration_samples);
//    
//    light_threshold = round(average*(100 + threshold_factor)/100); // Setter terskel
//    calibrating = false; //Går ut av kalibreringsmodus
//  }
//  // skrur av lys når kalibrering er ferdig
//  for (byte i=0; i<num_leds; ++i){
//    pixels.setPixelColor(i, pixels.Color(0, 0, 0));}
//  pixels.show();

  sei(); // Skrur på interrupts
}
int i;


void loop() {}


// ekstern interrupt for alle A pins
// 3I, 3E og 2I er koblet på A
ISR (PCINT0_vect) {
  //i % 2 ? digitalWrite(pin_axon, HIGH) : digitalWrite(pin_axon, LOW);
  //i++;
  // sjekker dendritt #3
  // eksitatorisk 
  if (digitalRead(pin_dendrite3E) == HIGH) {
    //if  (myMillis() - ts_dendrite1E_activated > dur_dendrite_reset) {
      ts_dendrite1E_activated = myMillis();
      //addPulse(true);
      potential += potential_dendrite_increment;
    //}
  }
  // inhibitorisk
  if (digitalRead(pin_dendrite3I) == HIGH) {
    if  (myMillis() - ts_dendrite3I_activated > dur_dendrite_reset) {
      ts_dendrite3I_activated = myMillis();
      //addPulse(false);
      potential -= potential_dendrite_decrement;
    }
  }

  // sjekker dendritt #2
  // inhibitorisk
  if (digitalRead(pin_dendrite2I) == HIGH) {
    if (myMillis() - ts_dendrite2I_activated > dur_dendrite_reset) {
      ts_dendrite2I_activated = myMillis();
      //addPulse(false);
      potential -= potential_dendrite_decrement;
    }
  }
}


//interrupt for alle B pins
// 1E, 1I og 2E er koblet på B
ISR(PCINT1_vect){
  //i % 2 ? digitalWrite(pin_axon, HIGH) : digitalWrite(pin_axon, LOW);
  //i++;
    // sjekker dendritt #1
    // eksitatorisk
  if (digitalRead(pin_dendrite1E) == HIGH) {
    if (myMillis() - ts_dendrite1E_activated > dur_dendrite_reset) {
      ts_dendrite1E_activated = myMillis();
      
      //addPulse(true);
      potential += potential_dendrite_increment;
    }
  } 
  // inhibitorisk
  if (digitalRead(pin_dendrite1I) == HIGH) {
    if (myMillis() - ts_dendrite1I_activated > dur_dendrite_reset) {
      ts_dendrite1I_activated = myMillis();
      //addPulse(false);
      potential -= potential_dendrite_decrement;
      }
  } 
  // sjekker dendritt #2
  // eksitatorisk
  if (digitalRead(pin_dendrite2E) == HIGH) {
    if (myMillis() - ts_dendrite2E_activated > dur_dendrite_reset) {
      ts_dendrite2E_activated = myMillis();
      //addPulse(true);
      potential += potential_dendrite_increment;
      }
  }  
   
}

// Timer interrupt A
ISR(TIM0_COMPA_vect){
  i % 2 ? digitalWrite(pin_axon, HIGH) : digitalWrite(pin_axon, LOW);
  i++;
  interrupt_counter++;
//  updatePulses();
//
//  // Er potensialet større eller mindre enn hvilepotensialet
//  if (potential > 0) {
//    if (potential >= potential_threshold) {
//      // Hvis potensialet er større enn terskelpotensialet legges en puls til i pulslisten
//      digitalWrite(pin_axon, HIGH);
//      fire = true;
//      // Og potensialet senkes til under hvilenivået
//      potential = -potential_threshold / 3; 
//    } else {
//      // Hvis potensialet er positivt, men ikke større enn terskelpotensialet skal det konvergere mot hvilepotensialet
//      potential *= potential_stabilize_factor;
//    }
//  } else if (potential < 0) {
//    // Begrenser potensialet så det ikke kan bli mindre enn negativ terkselspenning
//    if (-potential >= potential_threshold) {
//      potential = -potential_threshold;
//    }
//
//    // Konverger membranpotensialet mot hvilenivået
//    potential *= potential_stabilize_factor;
//  }
  
}
// Timer interrupt B
ISR(TIM0_COMPB_vect){
  if(fire){digitalWrite(pin_axon, LOW);
    fire = false;
  } 
}

// Hjelpefunksjoner
void addPulse(bool exitoric) {
  // skyver alle felter i listen ett hakk opp
  for (byte i=pulse_list_length - 1; i>0; --i) {
    pulse_list[i] = pulse_list[i - 1];
    pulses_type[i] = pulses_type[i - 1];
  }
  // skriver det nåværende tidspunktet til det første feltet i lista
  pulse_list[0] = myMillis();
  pulses_type[0] = exitoric;
  // sørger for at det første feltet i lista alltid er det siste
}

void updatePulses() {
  for (byte i=0; i<pulse_list_length; ++i) {
    if (pulse_list[i] > 0) {
      if (myMillis() - pulse_list[i] > dur_dendrite_delay) { // sjekker om pulsen har ventet lenge nok
        // resetter det nåværende feltet
        pulse_list[i] = 0;

        if (pulses_type[i]) { // eksitorisk puls
          potential += potential_dendrite_increment;
        } else { // inhibitorisk puls
          potential -= potential_dendrite_decrement;
        }
      } 
    } else {
      break;
    }
  }
}

long unsigned int myMillis(){
  return interrupt_counter;
}

