#include <avr/io.h>
#include <avr/interrupt.h>
void setup() {
  // put your setup code here, to run once:
  cli();
  
  TCCR0A = 0; // Timer control register normal operation
  TCCR0A = (1 << WGM01); // CTC-mode
  TCCR0B = 0b00000101;  // Pre-Scaler
  //TCCR0B |= ( (1 << CS02) || ( 1 << CS00));
  //TCCR0B |= (1 << CS00);
  OCR0A = 250; // Compare register
  TIMSK0 |= ( 1 << OCIE0A); // Enable interrupt på A

  pinMode(A0, OUTPUT);
  sei();
}

int i = 0;
int j = 0;
void loop() {
  // put your main code here, to run repeatedly:

}

ISR(TIM0_COMPA_vect){
  j++;
  if(j >31){
    if(i%2){digitalWrite(A0,HIGH);}
    else{digitalWrite(A0, LOW);}
    i++;
    j= 0;
  }
  
}




