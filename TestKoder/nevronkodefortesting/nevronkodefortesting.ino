// Kode for å teste felles inhbitorisk og eksitorisk inngang

// avr-libc library includes
#include <avr/io.h>

//Inkluderer NeoPixel-biblioteket som styrer LEDs
#include <Adafruit_NeoPixel.h>

// Definerer i/o-pins
#define pin_led 0 // LED-pin
#define pin_axon 1 //Akson-pin
#define pin_dendrite_E 2 // Dendrittpin Eksitorisk
#define pin_dendrite_I 3 // Dendrittpin Inhibitorisk

//

// Definerer konstanter for bruk med LEDs
#define num_dendrites 2 // Antall dendritter
#define num_leds 5 // Antall leds, teller ikke med dendritt-LEDs

// Definerer konstanter som har med membranpotensialet å gjøre
byte potential_threshold = 100;// Terskelpotensialet
byte potential_dendrite_increment = 80; // Hvor mye membranpotensialet skal stige når en puls treffer en eksitorisk dendritt
byte potential_dendrite_decrement = 80; // Hvor mye membranpotensialet skal synke når en puls treffer en inhibitorisk dendritt
byte potential_stabilize_speed = 50; // Hvor fort potensialet skal konvergere mot hvilenivået 
double potential = 0; // Membranpotensiale
#define potential_smoothness 0.1 // Hvor mye potensialet skal glattes
double potential_smooth = 0;

// Variabler som har med timing å gjøre
  // Variablene som begynner med dur_ er konstanter som har med varighet å gjøre (dur for duration) 
  // Variablene som begynner med ts_ er timestamps som brukes til timing
  // Alle tider er gitt i millisekunder


unsigned long int ts_pulse_fire = 0; // tidsstempel for siste fyring

byte dur_dendrite_delay = 255; // Tidsforsinkelse fra dendritten mottar et signal til det videreføres inn til cellekjernen
#define dur_axon_pulse 1


#define dur_dendrite_reset 5 // Minimum opphold mellom hver gang dendrittene kan aktiveres
// Variablene under holder styr på når dendrittene sist ble aktivert
unsigned long int ts_dendrite0_activated = 0; 
unsigned long int ts_dendrite1_activated = 0;
unsigned long int ts_dendrite2_activated = 0;

// Variabler som har med puls-listen å gjøre
  // Det er en tidsforsinkelse fra en puls treffer en dendritt til den blir registrert, dette er av pedagogiske årsaker
  // For at alle pulser skal registreres legges de til i en liste når de treffer dendritten, og fjernes når de registreres
  // Begge listene er volatile da de skal endres på under interrupt
#define pulse_list_length 10
volatile unsigned long int pulse_list[pulse_list_length]; // Pulslisten, den inneholder tidsstempler for alle pulser som dendritten har mottat, men som ikke har fyrt enda
volatile bool pulses_type[pulse_list_length]; // Listen som holder styr på om pulsene i pulslisten er inhibitoriske eller eksitoriske

// Initierer LEDs
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(num_leds + num_dendrites, pin_led, NEO_GRB + NEO_KHZ800);

void setup() {
  cli();// Skrur av interrupts
  
  // Setter opp i/o pins
  pinMode(pin_dendrite_E, INPUT);
  pinMode(pin_dendrite_I, INPUT);
  pinMode(pin_led, OUTPUT);
  pinMode(pin_axon, OUTPUT);
  
  // Skriver lav til alle output-pins fir å være sikker på at de oppfører seg som tiltenkt
  digitalWrite(pin_led, LOW);
  analogWrite(pin_axon, 0);

  // Skriver 0 til alle felter i puls-listen
  for (byte i=0; i<pulse_list_length; ++i) {
    pulse_list[i] = 0;
  }

  //Starter LED-styring
  pixels.begin();
  
  // Setter fargen på dendritt-LEDs
  // Alle dendritter er eksitoriske
  pixels.setPixelColor(num_leds, pixels.Color(0, 20, 0)); 
  pixels.setPixelColor(num_leds + 1, pixels.Color(0, 20, 0));
  
  //Setter fargen på resten av LEDsene
  for (byte i = 0; i < num_leds; ++i){
    pixels.setPixelColor(i, pixels.Color(0, 0, 0));
  }
  pixels.show();


  PCMSK0 = 0b00011100; // Skrur på interrupts på pin 2, 3 og 4   
  GIMSK = 0b00100000; // Skrur på pin change interrupts

  // Timer interrupt A
  TCCR0A = 0; // Timer control register normal operation
  TCCR0A |= (1 << WGM01); // CTC-mode
  TCCR0B = 0b00000101;  // Pre-Scaler = 1024
  OCR0A = 250; // Compare register
  TIMSK0 |= ( 1 << OCIE0A); // Enable interrupt på A
  // Interrupt frekvens =  ( 8MHz / Prescaler ) / OCR0A 
  

  sei(); // Skrur på interrupts
}

void loop() {
  
}

// ekstern interrupt
ISR (PCINT0_vect) {
  // sjekker dendritt #1
  // eksitatorisk 
  if (digitalRead(pin_dendrite_E) == HIGH) {
    if  (millis() - ts_dendrite0_activated > dur_dendrite_reset) {
      ts_dendrite0_activated = millis();
      addPulse(true);
      pixels.setPixelColor(num_leds, pixels.Color(0, 40, 0));        
    }
  }

  // sjekker dendritt #2
  if (digitalRead(pin_dendrite_I) == HIGH) {
    if (millis() - ts_dendrite1_activated > dur_dendrite_reset) {
      ts_dendrite1_activated = millis();
      addPulse(true);
      pixels.setPixelColor(num_leds + 1, pixels.Color(0, 40, 0));
    }
  }
 
}

// Timer interrupt
ISR(TIM0_COMPA_vect){
  
    updatePulses();

  // Er potensialet større eller mindre enn hvilepotensialet
  if (potential > 0) {
    if (potential >= potential_threshold) {
      // Hvis potensialet er større enn terskelpotensialet legges en puls til i pulslisten
      fireAxon();
      
      // Og potensialet senkes til under hvilenivået
      potential = -potential_threshold / 3; 
    } else {
      // Hvis potensialet er positivt, men ikke større enn terskelpotensialet skal det konvergere mot hvilepotensialet
      potential -= potential_stabilize_speed/1000.0;
      if (potential < 0) {
        potential = 0;
      }
    }
  } else if (potential < 0) {
    // Begrenser potensialet så det ikke kan bli mindre enn negativ terkselspenning
    if (-potential >= potential_threshold) {
      potential = -potential_threshold;
    }

    // Konverger membranpotensialet mot hvilenivået
    potential += potential_stabilize_speed/1000.0;
    if (potential > 0) {
      potential = 0;
    }
  }
  
  
}

// Hjelpefunksjoner
void addPulse(bool exitoric) {
  // skyver alle felter i listen ett hakk opp
  for (byte i=pulse_list_length - 1; i>0; --i) {
    pulse_list[i] = pulse_list[i - 1];
    pulses_type[i] = pulses_type[i - 1];
  }
  // skriver det nåværende tidspunktet til det første feltet i lista
  pulse_list[0] = millis();
  pulses_type[0] = exitoric;
  // sørger for at det første feltet i lista alltid er det siste
}

void updatePulses() {
  for (byte i=0; i<pulse_list_length; ++i) {
    if (pulse_list[i] > 0) {
      if (millis() - pulse_list[i] > dur_dendrite_delay) { // sjekker om pulsen har ventet lenge nok
        // resetter det nåværende feltet
        pulse_list[i] = 0;

        if (pulses_type[i]) { // eksitorisk puls
          potential += potential_dendrite_increment;
          pixels.setPixelColor(num_leds, pixels.Color(0, 20, 0));
          pixels.setPixelColor(num_leds + 1, pixels.Color(0, 20, 0));
        } else { // inhibitorisk puls
          potential -= potential_dendrite_decrement;
          pixels.setPixelColor(num_leds, pixels.Color(0, 20, 0));
          pixels.setPixelColor(num_leds + 1, pixels.Color(0, 20, 0));
        }
      } 
    } else {
      break;
    }
  }
}

void wait(unsigned int duration) {
  unsigned long int timestamp = millis();
  while(millis() - timestamp < duration) {};
}

void fireAxon() {
  digitalWrite(pin_axon, HIGH);
  wait(dur_axon_pulse);
  digitalWrite(pin_axon, LOW);
  ts_pulse_fire = millis();
}
