/*
http://thewanderingengineer.com/2014/02/17/attiny-i2c-slave/
*/
//#include <avr/io.h>


// Definerer konstanter for bruk med LEDs
#define num_leds 2 // Antall leds

// Definerer konstanter som har med membranpotensialet å gjøre
// Alle byte deklarasjoner er tenkt åpen for endring
byte potential_threshold = 25;// Terskelpotensialet [mv]
// Hvor mye membranpotensialet skal stige/synke når en puls treffer en dendritt
byte potential_dendrite_increment[3] = {80, 80, 80}; // Individuell vekting på hver inngang
// Forsinkelser på dendrittene
byte delay_dendrite[3] = {0, 0, 0}; // Dendritt 1, 2 og 3, forsinkelser i ms
// Grenser for potensialnivå
#define potential_lowerbound  -35
#define potential_upperbound  125
// Hvor fort potensialet skal konvergere mot hvilenivået aka tidskonstanten
double potential_stabilize_factor = 0.995;  //tau ca 50ms
byte potential_stabilize_factor_ms = 50; // Brukes til I2C
// Hvor fort potensialet skal konvergere mot hvilenivået dersom fyring
byte rechargeTime = 4; // rechargeTime/4 = n [ms]
byte rechargeCounter = rechargeTime;
byte recharge_drop = (byte)potential_lowerbound/2;
double potential_stabilize_factor_fire = (double)(((double)potential_upperbound-(double)recharge_drop)/(double)rechargeTime);
double potential = 0; // Membranpotensiale
#define potential_que_length 10
double potential_que[potential_que_length];
int potential_que_delay[potential_que_length];
int potential_que_index;


// periode for fyringsmekanismen gitt av fyreknappen
byte fire_period = 4*5; // 4*(n antall ms)
int fire_period_counter = 0;
// Om nevronet skal fyre
bool fire = 0;

// Variabler for logikk
// Om lyssensor er aktiv eller ikke
bool lightSensor = 0;
// terskel for lyssensor
int light_threshold;

// 12C konstanter
const int numberOfVariables = 9;
// Kommando definerer hva som skjer ved master request.
uint8_t command = 0;
#define I2C_SLAVE_ADDRESS 0x4   //
 bool transmission;



#define I2C_SLAVE_ADDRESS 0x4   //this slave address (0x1, 0x2, 0x3)
#include <TinyWireS.h>          //the ATTiny Wire library
int LEDPin = A0;                //indicator LED
int counter1;
int counter2;

void setup() {
  cli();
  // Kobler seg på bussen som slave
  TinyWireS.begin(I2C_SLAVE_ADDRESS); 
  // Argumentet sier hvilket funskjon som kalles når slaven blir addressert på busen
  TinyWireS.onReceive(receiveEvent);
  TinyWireS.onRequest(requestEvent);
  pinMode(LEDPin, OUTPUT);
  TCCR0A = 0; // Timer control register normal operation
  TCCR0A |= (1 << WGM01); // CTC-mode
  TCCR0B = 0b00000100;  // Pre-Scaler = 256
  OCR0A = 32; // Compare register A
  OCR0B = 8; // Compare register B
  TIMSK0 = 0b00000110;
  sei();

}

int i = 0;

void loop() {
  TinyWireS_stop_check(); // Foreløpig mystisk, men nødvendig
}

void requestEvent(){
  transmission = true;
  if(command == 100){
    // TinyWire støtter ikke sending av flere bytes i samme funksjon
    TinyWireS.send(potential_stabilize_factor_ms);
    TinyWireS.send(potential_threshold);
    TinyWireS.send(recharge_drop);
    for(int i = 0; i < 3; i++){TinyWireS.send(delay_dendrite[i]);}
    for(int i = 0; i < 3; i++){TinyWireS.send(potential_dendrite_increment[i]);}
  } 
  command = -1; // Markerer utført commando
  transmission = false;
}

// regner ut potential_stabilize_factor gitt at interrupt perioden er 0.25ms
void setTimeconstant(int tau){
  potential_stabilize_factor_ms = tau;
  potential_stabilize_factor = exp(-1/(4*tau));
}

void receiveEvent(uint8_t howMany){
  transmission = true;
  // Sjekker om master PINGer eller sender kommando
  if(TinyWireS.available() > 0){
    // Kommando sendt
    command = TinyWireS.receive(); // Første byte er kommando. Hva skal gjøres.
    byte val = TinyWireS.receive();// evt verdi som skal settes
    if( (0 <= command) && (command < 6) ) {
      switch(command){
        case 1:
          setTimeconstant(val);
          break;
        case 2:
          potential_threshold = val;
          break;
        case 3:
          recharge_drop = val;
          break;
        case 4:
          potential_dendrite_increment[0] = val;
          break;
        case 5:
          potential_dendrite_increment[1] = val;
          break;
        case 6:
          potential_dendrite_increment[2] = val;
          break;
        case 7:
          delay_dendrite[0] = val;
          break;
        case 8:
          delay_dendrite[1] = val;
          break;
        case 9:
          delay_dendrite[2] = val;
          break;
      }
    }    
  }
  while(TinyWireS.available()){TinyWireS.receive();}
  transmission = false;
}



ISR(TIM0_COMPA_vect){
  counter2++;
  if(counter2>=37){
    if(counter1%2){digitalWrite(LEDPin,HIGH);}
    else{digitalWrite(LEDPin, LOW);}
    counter1++;
    counter2= 0;
  } 
}

ISR(TIM0_COMPB_vect){
   
}

