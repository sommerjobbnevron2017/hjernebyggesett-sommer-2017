
#define pin_led 3 // LED-pin
#define pin_axon 5 //Akson-pin
#define pin_dendrite1E 10 // Dendrittpin 1 eksitorisk
#define pin_dendrite1I 9 // Dendrittpin 1 inhibitorisk
#define pin_dendrite2E 8 // Dendrittpin 2 eksitorisk
#define pin_dendrite2I 7 // Dendrittpin 2 inhibitorisk
#define pin_dendrite3E 1 // Dendrittpin 3 eksitorisk
#define pin_dendrite3I 0 // Dendrittpin 3 inhibitorisk

void setup() {
  cli();// Skrur av interrupts
  
  // Setter opp i/o pins
  pinMode(pin_dendrite1E, INPUT);
  pinMode(pin_dendrite1I, INPUT);
  pinMode(pin_dendrite2E, INPUT);
  pinMode(pin_dendrite2I, INPUT);
  pinMode(pin_dendrite3I, INPUT);
  pinMode(pin_dendrite3E, INPUT);
  pinMode(pin_led, OUTPUT);
  pinMode(pin_axon, OUTPUT);
  
  PCMSK0 = 0b10000011; // Skrur på interrupts på pin A0, A1 og A7 
  PCMSK1 = 0b00000111; // Skrur på interrupts på pin B0, B1 og B2  
  GIMSK  = 0b00110000; // Skrur på pin change interrupts

  // Timer interrupt A
  TCCR0A = 0; // Timer control register normal operation
  TCCR0A |= (1 << WGM01); // CTC-mode
  TCCR0B = 0b00000101;  // Pre-Scaler = 1024
  OCR0A = 250; // Compare register
  TIMSK0 |= ( 1 << OCIE0A); // Enable interrupt på A
  // Interrupt frekvens =  ( 8MHz / Prescaler ) / OCR0A 
  

  sei(); // Skrur på interrupts

}
int i = 0;
void loop() {
  // put your main code here, to run repeatedly:

}

ISR(TIM0_COMPA_vect){
  if(i%2){
   // digitalWrite(pin_axon, HIGH);
  }else{//digitalWrite(pin_axon, LOW);
    }
 // i++;
}

ISR(PCINT0_vect){
  if(i%2){
    digitalWrite(pin_axon, HIGH);
  }else{digitalWrite(pin_axon, LOW);}
  i++;
}

ISR(PCINT1_vect){
  if(i%2){
    digitalWrite(pin_axon, HIGH);
  }else{digitalWrite(pin_axon, LOW);}
  i++;
}

