/*
  Goal ... have the ATTiny84 join an I2C network as a slave 
  with an Arduino UNO as the I2C network master

  using the information from 
  http://thewanderingengineer.com/2014/02/17/attiny-i2c-slave/
  as the starting point.

*/

const int numberOfVariables = 6;
// Kommando definerer hva som skjer ved master request.
uint8_t command = 0; 

// Alle variabler må lagres i en liste
byte variables[numberOfVariables] = {70,66,69,90,91,93}; 
//#1 Tidskonstant
//#2 Propageringskonstant
//#3 Terskelspenning
//#4 Vekt_dendritt1
//#5 Vekt_dendritt2
//#6 Vekt_dendritt3


#define I2C_SLAVE_ADDRESS 0x4   //this slave address (0x1, 0x2, 0x3)
#include <TinyWireS.h>          //the ATTiny Wire library
int LEDPin = A0;                //indicator LED


void setup() {
  // Kobler seg på bussen som slave
  TinyWireS.begin(I2C_SLAVE_ADDRESS); 
  // Argumentet sier hvilket funskjon som kalles når slaven blir addressert på busen
  TinyWireS.onReceive(receiveEvent);
  TinyWireS.onRequest(requestEvent);
  pinMode(LEDPin, OUTPUT);
  
}

int i = 0;

void loop() {
  TinyWireS_stop_check(); // Foreløpig mystisk, men nødvendig
}

void requestEvent(){
  if(command == 100){
    // TinyWire støtter ikke sending av flere bytes i samme funksjon
    for(int i = 0; i < numberOfVariables; i++){
      TinyWireS.send(variables[i]);
    }
  } 
  command = -1; // Markerer utført commando
}


void receiveEvent(uint8_t howMany){
  // Sjekker om master PINGer eller sender kommando
  if(TinyWireS.available() > 0){
    // Kommando sendt
    command = TinyWireS.receive(); // Første byte er kommando. Hva skal gjøres.
    byte val = TinyWireS.receive();// evt verdi som skal settes
    if( (0 <= command) && (command < 6) ) {
      variables[command] = val;
    }    
  }
  while(TinyWireS.available()){TinyWireS.receive();}
}

