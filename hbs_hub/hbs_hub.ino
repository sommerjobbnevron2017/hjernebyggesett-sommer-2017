#include <Wire.h>

////////////////////////////////
// Kode som skal kjøre på hub //
////////////////////////////////

void setup() {
  // Starter kommunikasjon med I2C
  Wire.begin();

  // Starter kommunikasjon over USB
  Serial.begin(9600);
}

void loop() {
  // Hver instruksjon til huben består av 4 bytes, venter til det er så mange bytes i bufferen før den begynner 
  if (Serial.available() > 3) { 
    
    // Første byte er kommando
    byte instr_command = Serial.read();
    
    // Andre kommando er id-en til nevronet
    byte instr_id = Serial.read();

    // Tredje er parameteret til kommandoen
    byte instr_parameter = Serial.read();

    // Fjerde er hvor mange bytes som skal sendes i retur
    byte instr_return = Serial.read();
    // (I vårt tilfelle er dette alltid antall variabler, men kjekkere om dette kan styres fra klienten)
    
    if (instr_return > 0) {
      // Pinger nevron
      Wire.beginTransmission(instr_id); 
      
      // Lagrer resultatet av ping
      byte response = Wire.endTransmission(); 
      
      // Sender resultatet over USB
      Serial.write(response); 
      
      // Hvis alt gikk bra med ping så krever den variabler fra nevronet
      if (response == 0) {
        int returnNum = Wire.requestFrom(instr_id, instr_return); // Forventer å få
        delay(1);
        
        while (Wire.available() > 0) {
          Serial.write(Wire.read());
        }
      }
    } else {
      switch (instr_command) {
        
        case 255: // Ping
        Wire.beginTransmission(instr_id);
        break;
        
        default:
        Wire.beginTransmission(instr_id);
        Wire.write(instr_command);
        Wire.write(instr_parameter);
      }

      // Skriver resultatet av sendingen til nevronet over USB
      Serial.write(Wire.endTransmission());
    }
  }
}

