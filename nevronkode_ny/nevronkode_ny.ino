#include <avr/io.h> // Register navn etc
#include <TinyWireS.h> // I2C
#include <Adafruit_NeoPixel.h> // LED håndtering, krever 8Mhz klokkehastighet
#include <EEPROM.h> // Flash lagring
#include <math.h> // for exp funskjonen

//Arduino IDE & Pin Mapping 
// ATMEL ATTINY84 / ARDUINO
//
//                           +-\/-+
//                     VCC  1|    |14  GND
//             (D 10)  PB0  2|    |13  AREF (D  0)
//             (D  9)  PB1  3|    |12  PA1  (D  1) 
//             RESET   PB3  4|    |11  PA2  (D  2) 
//  PWM  INT0  (D  8)  PB2  5|    |10  PA3  (D  3) 
//  PWM        (D  7)  PA7  6|    |9   PA4  (D  4) 
//  PWM        (D  6)  PA6  7|    |8   PA5  (D  5)        PWM
//                           +----+
//        PINMAP
//IDE    Attiny84   Physical Pin
//  0      PA0           13
//  1      PA1           12
//  2      PA2           11
//  3      PA3           10
//  4      PA4            9
//  5      PA5            8
//  6      PA6            7
//  7      PA7            6
//  8      PB2            5
//  9      PB1            3
// 10      PB0            2

////// NB NB NB NB //////// !!!
//
// PROGRAMKODEN AVHENGER AV AT TIMERINTERRUPTET
// SKJER 4 GANGER HVERT MILLISEKUND. ENDRES DETTE
// VIL IKKE LENGER NEVRONET OPPFØRE SEG SOM TILTENKT
// DET 1ms ELLER 1 Hz IKKE LENGER VIL VÆRE 1ms ELLER
// 1 Hz I PRAKSIS. KODEN VIL DOG FORTSATT FUNGERE
//
////// NB NB NB NB //////// !!!


////////////////////
// 12C konstanter //
////////////////////
#define I2C_SLAVE_ADDRESS 2    // Nevronadresse
#define number_of_variables 12  // Antall justerbare parametere

////////////////////////////
// I/O PINS Deklarasjoner //
////////////////////////////
#define pin_led 10 // LED-pin ( Må være på PB0 )
#define pin_sensor 5 // Lyssensor pin ( Må være på port A siden port B ikke har ADC. Trenger "analoge" verdier )
#define pin_axon 9 //Akson-pin
#define pin_dendrite1E 1 // Dendrittpin 1 eksitorisk
#define pin_dendrite1I 0 // Dendrittpin 1 inhibitorisk
#define pin_dendrite2E 3 // Dendrittpin 2 eksitorisk
#define pin_dendrite2I 2 // Dendrittpin 2 inhibitorisk
#define pin_dendrite3E 7 // Dendrittpin 3 eksitorisk
#define pin_dendrite3I 8 // Dendrittpin 3 inhibitorisk

// Variabler for å kunne forenkle oppsettkode
// Kunne kunne vært lokalt i setup funksjonen med globaldeklarasjon brukte mindre programminne.
// Egentlig unødvendig, men å sette opp pinsene med en for-løkke sparte programminne
#define number_of_outputs 2
const uint8_t output_pins[number_of_outputs] = {10, 9};
#define number_of_inputs 7
const uint8_t input_pins[number_of_inputs] = {1, 0, 3, 2, 7, 8, 5};

// Definerer konstanter for bruk med LEDs
#define number_of_leds 7 // Antall leds
volatile int counter_led; // Støtteteller for blink ved fyring

// Definerer konstanter som har med membranpotensialet å gjøre
// Grenser for potensialnivå
#define potential_lowerbound  -35
#define potential_upperbound  125

// Hvor fort potensialet skal konvergere mot hvilenivået aka tidskonstanten
volatile double potential_stabilize_factor = 0.995;  //tau ca 50ms

// Hvor fort potensialet skal konvergere mot hvilenivået dersom fyring
// Ikke justerbar, men må allikevel skaleres i forhold til slowmotion modus
volatile byte time_decharge = 4; // time_decharge/4 = n [ms]

volatile byte counter_decharge = time_decharge; // Støtteteller for utladning ved fyring
volatile double potential = 0; // Selve membranpotensialet
// Alle justerbare parametere er lagt til i vektoren under. Dette er mindre lesbart, men mer effektivt og krever mindre kode for bla. I2C støttefunskjonene
volatile byte variables[number_of_variables] = {50, 25, (byte)(-potential_lowerbound/2), 23, 23, 23, 10, 10, 10, 0, 0, 1};
// Liste for variabel grensene. Brukes for å sikre at variablene alltid innenfor et gyldig nivå
const uint8_t limits[number_of_variables][2] = {{1,50}, {5, 45}, {0, 39}, {1, 50}, {1, 50}, {1, 50}, {0, 100}, {0, 100}, {0, 100}, {0, 100},{0, 100}, {1, 20}};

/* Justerbare parametere og tilhørende indeks
 *  0. Tidskonstant    (potential_stabilize_factor) (SPESIAL)
 *  1. Terskelspenning (potential_threshold)
 *  2. Post-fyring stabiliseringsspenning (recharge_drop). Har en offset på 35
 *  3 - 5. Vekting på innganger (potential_dendrite_increment[3])
 *  6 - 8. Forsinkelse på innganger (delay_dendrite[3])
 *  9. Spontanfyringsfrekvens
 *  10. Frekvensen til fyringen når lys treffer sensor ( 0 = sensor av)
 *  11. Slowmotion (SPESIAL)
 *  SPESIAL = Kreves mer enn å bare oppdatere verdien i variables for å oppnå tiltenkt funksjon.
 *  */

volatile unsigned int counter_fire = 0; // Støtteteller for å oppnå riktig fyringsfrekvens ved aktivering av lyssensor

// Siden det ikke er nokke programminne til å implentere tellere av typen 'long' har vi måttet trikse litt for at tellerene ikke skal gå i overflyt
// Worst-case er counter_spontanious = 2*variables[11]*frequencyToMillis(variables[9]); = 2*20*8000 = 320 000. 320 000/5 = 64 000 og vil gå innenfor
// rekkevidden til 16-bit unsigned int. Vi skalerer derfor returverdien til frequencyToMillis med fem og oppdaterer tellerene fem ganger så sjeldent.
volatile int overflow_stop;
const int pre_scale = 5;

// Variabler for logikk 
volatile int threshold_light; // Terskel for lyssensor. Settes av kalibrering i setup
volatile bool button_pushed = 0;  // Brukes for å oppnå én enkelt fyring ved trykk av fyreknapp
volatile bool status_fire = 0; // Om nevronet er i fyringsmodus (nevronet fyrer)
volatile byte prev_slow = 1; // Forrige slowmotion verdi
volatile byte spont_fire = 0; // Fyring som følge av spontanfrekvens

// Teller for spontanfyring. Frekvensen halveres siden vi ønsker 0.5Hz - 50 Hz ( Variablene lagres som heltall(byte))
volatile unsigned int counter_spontanious = variables[11]*frequencyToMillis((variables[9]));

// Disse variablene sørger for å få riktig forsinkelse på inngangene
#define potential_que_length 101
volatile int8_t potential_que[potential_que_length]; // Liste over potensialbidrag som venter på å bli lagt til.
volatile byte potential_que_index; // Index for potential_que. Oppdateres hvert 10ms. Sørger for å iterere igjennom liste med faste mellomrom
volatile uint16_t potential_que_counter_sup; //Støtteteller som sammen med limitier_que sørger for at indeksen til listen bare økes hvert 10 ms.
volatile int limiter_que = 40;  // Brukes for modulus i timerinterrupt. Kunne vært hardkodet hadde det ikke vært for slow motion modus

// Siste samplede verdi på sensor
volatile uint16_t sample_sensor;

// Initierer LEDs
Adafruit_NeoPixel LEDstrip = Adafruit_NeoPixel(number_of_leds, pin_led, NEO_GRB + NEO_KHZ800);

void setup(){
  cli();// Skrur av interrupts
  
  ///// EEPROM ///// ( For at mikrokontrolleren ikke skal glemme de siste instillingene)
  // Om mikrokontrolleren nylig er omprogrammert må verdiene i EEPROMet settes til standard verdiene
  // Om dette er tilfelle vil alle EEPROM register leses som 255
  if(EEPROM.read(0) == 255){ // Alle EEPROM verdier leses som 255 før de har blitt skrivet til
    // Standard verdier settes
    for(int i = 0; i < number_of_variables; ++i){ // Alle verdiene i variables
      EEPROM.update(i, variables[i]); 
    }
  }else{
    // Mikrokontroller er ikke nylig omprogrammert. Verdier leses inn.
    for(int i = 0; i < number_of_variables; ++i){
      variables[i] = EEPROM.read(i);
    }
    potential_stabilize_factor = setTimeconstant(((double)(variables[11]*variables[0])));
    setSlowmotion(variables[11]);
  }
  
  ////// I/O //////////
  // Setter opp i/o pins
  for(int i = 0; i < number_of_outputs; ++i){
    pinMode(output_pins[i], OUTPUT);
  }
  for(int i = 0; i < number_of_inputs; ++i){
    pinMode(input_pins[i], INPUT);
  }
//  // Skriver lav til alle output-pins for å være sikker på at de oppfører seg som tiltenkt
  digitalWrite(pin_led, LOW);
  digitalWrite(pin_axon, LOW);

  //// I2C SETUP ////
  // Kobler seg på bussen som slave
  TinyWireS.begin(I2C_SLAVE_ADDRESS); 
  // Argumentet sier hvilket funskjon som kalles når slaven blir addressert på busen
  TinyWireS.onReceive(receiveEvent);
  TinyWireS.onRequest(requestEvent);

  
  //// INTERRUPT SETUP /////
  // Register for å bestemme hvilke pins en vil ha interrupts på. Dette gjelder da alle dendrittene.
  // Interrupts for I2C pinnene blir håndtert av støttebiblioteket TinyWireS 
  PCMSK0 = 0b10001111; // Skrur på interrupts på pin A0, A1, A2, A4 og A7 
  PCMSK1 = 0b00000100; // Skrur på interrupts på pin B0
  // Generelt interrupt register som aktiverer interruptesene over
  GIMSK  = 0b00110000; 
  // Timer interrupt A
  TCCR0A = 0b00000010; // Timer control register CTC-mode (Clear timer on compare, brukes for å få timer interrupt ved faste intervall)
  TCCR0B = 0b00000010;  // Pre-Scaler = 8
  OCR0A = 250; // Compare register A
  TIMSK0 |= ( 1 << 1);
  // Interrupt frekvens =  ( 8MHz / Prescaler ) / OCR0A

  ///// Sensor kalibrering
  int sample_temp;
  for(int i = 0; i < 10; ++i){
    sample_temp += analogRead(pin_sensor);
    delay(100);
  }
  threshold_light = (int)sample_temp/10 + 100;
  
  ///// Neopixel initsialisering
  LEDstrip.begin();
  sei(); // Skrur på interrupts
}

// I mellom interruptene vil mikrokontrolleren oppdatere LEDstripen og sample fra lyssensoren så fort den kan
void loop(){
  TinyWireS_stop_check(); // Foreløpig mystisk, men nødvendig
  refreshLeds();
  sample_sensor = analogRead(pin_sensor);
  refreshLeds();
}
 
///////////////////////
// Støtte funskjoner //
///////////////////////

// Arduinos egen map tok for stor plass. Denne funksjonen går ut i fra at nedre grense, både før og etter, alltid er 0
int my_map(int val, int old_upper, int new_upper){
  return (int)(val*new_upper/old_upper);
}

// Mindre programminne krevdes ved å ikke bruke arduinos innebygde constrain funksjon
int my_constrain(int val, int lower, int upper){
  if(val > upper){return upper;}
  else if(val < lower){return lower;}
  else{return val;}
}

// LEDsene til nevronet er programmert til å representere membranpotensialet til nevronet
void refreshLeds(){
  if(counter_led != 0){ // Fyring = blink
    for(int i = 0; i < number_of_leds; ++i){
    LEDstrip.setPixelColor(i, 0, 0, 255);
  }
  }else{ // Oppdaterer ledstripen til å representere membranpotensialet
    double potential_temp = potential;
    LEDstrip.setPixelColor(3, 70, 70, 70);
    if((int)potential >= 0){
      for(int i = 0; i < 3; ++i){
        int color = my_constrain( (int)potential_temp - (i)*(int)variables[1]/3, 0, variables[1]/3);
        color = my_map(color, variables[1]/3, 60);
        LEDstrip.setPixelColor(i + 4, (int)color*(100-(i+1)*33)/100, (int)((3+(i+1)*0.33)*color), (int)color*(100-(i+1)*33)/100);
        LEDstrip.setPixelColor(2-i, 0, 0, 0);
      }
    }else{
      for(int i = 0; i < 3; ++i){
        int color = my_constrain( -((int)potential_temp - (i)*potential_lowerbound/3), 0, -potential_lowerbound/3);
        color = my_map(color, -potential_lowerbound/3, 60);
        LEDstrip.setPixelColor(2-i , (int)((3+(i+1)*0.33)*color), (int)color*(100-(i+1)*33)/100, (int)color*(100-(i+1)*33)/100);
        LEDstrip.setPixelColor(i+4, 0, 0, 0);
      }
    }
  }
  LEDstrip.show();
}

// regner ut potential_stabilize_factor (tidskonstanten) gitt at interrupt perioden er 0.25ms
double setTimeconstant(double tau){return exp(-1.0/(4.0*tau));}

// Oppdaterer potensialet. Denne funksjonen tar inn hvilken dendritt som har mottatt stimuli og hvilken egenskap den har.
// Oppdaterer deretter potential_que listen og sørger for riktig tidsforsinkelse ut i fra hva som er satt i "variables"
void updatePotential(int dendrite, bool dendrite_mode){
  byte index = variables[dendrite + 5] + potential_que_index;
  if((int)index >= potential_que_length){index -= potential_que_length;} // Indekseringskorrigering
  // eksistatorisk
  if(dendrite_mode){
    // Overflyt sikring
    if( (potential_que[index] + (double)variables[dendrite +2]) > 127){potential_que[index] = 127;}
    else{potential_que[index] += (double)variables[dendrite+2];}
  }
  // inhbitorisk
  else{
    if( (potential_que[index] - (double)variables[dendrite +2]) < -128){potential_que[index] = -128;}
    else{potential_que[index] -= (double)variables[dendrite+2];}
  }
}

// Selvbeskrivende. Alle variabler som har noe med tid og gjøre (tellere etc) må oppdateres
void setSlowmotion(int slow){
  double new_fac = (double)(slow/prev_slow);
  limiter_que *= (int)(new_fac);
  counter_fire *= (int)((new_fac));
  time_decharge *= (byte)((new_fac));
  counter_decharge *= (byte)((new_fac));
}

// Kalles dersom nevronet skal fyre
void fireAxon(){
  status_fire = true;
  digitalWrite(pin_axon, HIGH);
  potential = potential_upperbound;
  counter_led = 450; // Bestemmer hvor lenge utgangen skal være høy
}

// Regner om fra frekvens til periode lik 4*x antall millisekund for bruk av støttetellere som oppdateres 4 ganger hvert millisekund
// Funksjonen brukes for å sette spontanfyringsfrekvens og frekvens ved lyssensor. 1 = 0.5Hz og argumentet dobles derfor når en bruker
// funskjonen i sammenheng med lyssensor telleren.
int frequencyToMillis(byte freq){
  if(freq == 0){return 0;}
  else{return (int)(8000/(pre_scale*freq));}
}

bool updatePotential(){
  bool fired = 0;
  if(!(potential_que_counter_sup % limiter_que)){ // Iterer igjennom potensial kø listen kun hvert 10ms
    if(!status_fire){ // Ikke fyringsmodus
      potential += (double)(potential_que[potential_que_index] + variables[1]*spont_fire); // Oppdaterer potensialet
      if(potential >= (double)variables[1]){ 
        // start fyring
        fireAxon();
        fired = 1;
      }else if(potential < potential_lowerbound){potential = potential_lowerbound;} // Utenfor gyldig område
    }
    /* Ignorer all stimuli som ellers hadde bidratt til potensiale under fyring.
    Itereringen av listen kan heller ikke opphøre bare fordi nevronet fyrer.*/
    potential_que[potential_que_index++] = 0; // Øker teller og nuller verdi som er lagt til potensialet
    if(potential_que_index == potential_que_length){potential_que_index = 0;} // Utenfor index
    spont_fire = 0;
  }
  return fired;
}

////////////////////////
// Interrupt handlers //
////////////////////////

// ekstern interrupt for alle A pins
// 1I, 1E, 2E, 2I og 3E er koblet på port A
ISR(PCINT0_vect){
  // Kun interessert i hva som er på inngangene om lyssensor ikke er aktiv
  if(!variables[10]){
  // Sjekker hvilken pin som utløste interruptet
  if(digitalRead(pin_dendrite1I) == HIGH){updatePotential(1, false);}
  if(digitalRead(pin_dendrite1E) == HIGH){updatePotential(1, true);}
  if(digitalRead(pin_dendrite2I) == HIGH){updatePotential(2, false);}
  if(digitalRead(pin_dendrite2E) == HIGH){updatePotential(2, true);}
  if(digitalRead(pin_dendrite3E) == HIGH){updatePotential(3, true);}
  }
}

//interrupt for alle B pins
// 3I koblet på port B
ISR(PCINT1_vect){
  // Kun interessert i hva som er på inngangene om lyssensor ikke er aktiv
  if(!variables[10]){
  if(digitalRead(pin_dendrite3I) == HIGH){updatePotential(3, false);}
  }
  // Selv om det kun er én pin i denne handleren så må en allikevel sjekke om pinen leser høy.
  // Dette er fordi at handlerene kalles ved enhver logisk endring så den kan ha gått fra høy til lav.
}  

// Timer interrupt A. Mesteparten av nevronoppførselen er knyttet til hva som skjer i denne funksjonen
ISR(TIM0_COMPA_vect){
  // (In/de)krementerer støttetellere
  potential_que_counter_sup++;
  overflow_stop--;
  if(counter_led != 0){counter_led--;}
   
  // Spontanfyring
  if(variables[9] != 0){
    if(counter_spontanious == 0){
      spont_fire = 1; // Legger til en 'terskelspenning' til potensiale ved neste potensialoppdatering
      counter_spontanious = variables[11]*frequencyToMillis(variables[9]);
    }else if(!(overflow_stop % pre_scale)){counter_spontanious--;}
  }
    
  //////////////////
  // FYRINGSMODUS //
  //////////////////
  if(status_fire){
    updatePotential();
    counter_decharge--; // Nevronet skal være i fyringsmodus i 1ms. Telleren når 0 når det har skjedd.
    if(counter_decharge < 1){
      // Ferdig å fyre.
      counter_decharge = time_decharge;
      status_fire = false;
      potential = (double)variables[2] - 35; // Potensialet er max under fyring og settes nå tilbake til en gitt verdi. (Variabelen forkyvet med 35 for å håndtere negative verdier)
      digitalWrite(pin_axon, LOW);
    }
  }
  /////////////////
  // NORMALMODUS //
  /////////////////
  else{  
    potential *= potential_stabilize_factor; // Eksonentiell utladning
    if(updatePotential()){return;}
    
    // Sjekker om fyringsknapp er trykket
    if((sample_sensor > 1020) && !button_pushed){
      button_pushed = true;
      fireAxon();
      return;
    }else if( (sample_sensor <= 1000) && button_pushed){button_pushed = false;}
    // Lyssensor
    if(counter_fire == 0){
      if( ( (sample_sensor > threshold_light) && (variables[10]) && (sample_sensor < 1000) )){
        fireAxon();
        counter_fire = variables[11]*frequencyToMillis(2*variables[10]);
      }
    }else if(!(overflow_stop % pre_scale)){counter_fire--;}
  } // end else
}// end Timer


////////////////////
// I2C Funksjoner //
////////////////////

// Kalles når master ber om data
void requestEvent(){
  // TinyWire støtter ikke sending av flere bytes i samme funksjon
  for(int i = 0; i < number_of_variables; ++i){
    TinyWireS.send(variables[i]);
  }
}

// Kalles når master snakker til slave (nevronet)
void receiveEvent(uint8_t howMany){
  // Sjekker om master PINGer eller sender kommando
  if(TinyWireS.available() > 0){
    // Kommando sendt
    byte command = TinyWireS.receive(); // Første byte er indeks/kommando.
    byte val = TinyWireS.receive();// evt verdi som skal settes
    if( (-1 < command) && (command < number_of_variables) ){  // Sikrer at vi har fått en gyldig indeks.
      if(((int)val >= (int)limits[command][0]) && ((int)(val) <=  limits[command][1])){// Sikrer at verdien er innenfor gydlig intervall
        
        variables[(int)command] = val;  // Oppdaterer variabel
        EEPROM.update((int)command, val);  // Oppdaterer EEPROM
        
        //  Andre Spesialtilfeller
        if(command == 0){potential_stabilize_factor = setTimeconstant(((double)(variables[11]*variables[0])));}  // Tidskonstant
        else if(command == 0b00001011){  // Oppdater slow-motion tilstand
          prev_slow = variables[11];
          setSlowmotion((int)variables[11]);
          potential_stabilize_factor = setTimeconstant(((double)(variables[11])*(double)(variables[0])));
        }
        else if(command == 0b00001001){counter_spontanious = (int)(variables[11]*frequencyToMillis(variables[9]));}  // Spontanfyringsfrekvens
        else if(command == 0b00001010){counter_fire = (int)(variables[11]*frequencyToMillis(2*variables[10]));}     // Lyssensor frekvens
      }
    }
  }
  // Om det med en feil skulle være flere bytes igjen på BUSen
  while(TinyWireS.available()){TinyWireS.receive();}
}
